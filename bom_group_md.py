#
# Python script to generate a BOM from a KiCad generic netlist
# in Markdown format
#
# Sorted and Grouped Markdown BOM with advanced grouping
# same as version from KiCad install but without footprints

"""@package
    Generate a Markdown BOM list.
    Components are sorted and grouped by value
    Fields are (if exist)
    Ref, Quantity, Value, Part, Description, Vendor
    Command line:
    python "pathToFile/bom_group_md.py" [--todocs] [--rlist] [--groups GROUPS] netlist-file markdown-file
    If --todocs is used, replace path for output file with
    /project/path/Docs/BOM where /project/path is the specified or
    implicit directory or one at a higher level that contains a directory 
    Docs. If no such Docs directory is found, specified or implicit path is 
    used instead. BOM directory is created if it does not exist.
    If --rlist is used, append list of resistors in color band order.
    If --groups is used, parse `GROUPS` to map refs ranges to groups that
    are listed separately:
    e.g. if `GROUPS` is (("Front", (0,100),(300,400)),("Back", (100,300))) then refs in [0,100) and [300,400) are in one group called "Front", refs in [100,300) are in the other called "Back".

    /usr/bin/python3 "/home/rsholmes/.local/share/kicad/7.0/plugins/bom_group_md.py" --todocs --rlist "%I" "%O_bom.md"
"""

# Import the KiCad python helper module and the csv formatter
import kicad_netlist_reader
import sys
import os.path
import re
import argparse

def str2tuple (s):
    """
Take string representation of tuple and convert it to a tuple
Entries enclosed in quotes are kept as such, others are attempted to convert to int
Limitation: '~' must not be present
"""
    
    s = s.strip()
    if s[0] != '(':
        if (s[0]=="'" and s[-1]=="'") or (s[0]=='"' and s[-1]=='"'):
            return s[1:-1]
        else:
            try:
                return int(s)
            except:
                return s
    s = s[1:-1]
    level = 0
    s2 = ''
    for ss in s:
        if ss=='(':
            level += 1
        elif ss==')':
            level -= 1
        elif ss==',' and level==0:
            ss = '~'
        s2 += ss
    as2 = s2.split('~')
    ret = []    
    for a in as2:
        ret.append (str2tuple(a))
    return tuple(ret)

def ingroup (n, rng):
    """
See if `n` is in any of the ranges specified by tuple of duples `rng`
"""
    for r in rng:
        if n >= r[0] and n < r[1]:
            return True
    return False
    
def findDocs (path):
    """
    If path contains a directory named Docs, return relative path to
    that directory, otherwise recursively look in higher
    directories. If no such Docs directory is found return None.
    """

    if path == os.path.sep:
        return
    if path == "":
        path = os.getcwd()
    for p in os.listdir(path):
        if p == "Docs" and os.path.isdir(os.path.join(path, p)):
            return os.path.relpath(os.path.join(path, p))
    return findDocs (os.path.dirname(path))    
    
def myEqu(self, other):
    """myEqu is a more advanced equivalence function for components which is
    used by component grouping. Normal operation is to group components based
    on their Value and Footprint.
    In this example of a more advanced equivalency operator we also compare 
    several custom fields.
    """
    cfields = ["Tolerance", "Manufacturer", "Part", "Vendor", "SKU", "Voltage", "Note"]
    result = True
    if self.getValue() != other.getValue():
        result = False
    #elif self.getPartName() != other.getPartName():
    #    result = False
    elif self.getFootprint() != other.getFootprint():
        result = False
    else:
        for cf in cfields:
            if self.getField(cf) != other.getField(cf):
                result = False
                break

    # If all are the same except PartName it's probably a mistake
    
    if result and self.getPartName() != other.getPartName():
        print (f"*** Same except PartName: {self.getRef()}, {other.getRef()}")
    return result

"""
Format list of references
>= 3 consecutively numbered references are replaced with a range
"""
def ref_format (r):

    rs = []  # List of [prefix string, number]
    for ri in r:
        rg = re.search(r"^(.*[^0-9?])([0-9?]*?)$", ri)
        nn = 0 if "?" in rg.group(2) else int(rg.group(2)) # if un-annotated
        rs.append ([rg.group(1), nn])
    i = 0
    rss = [] # List of ranges or single refs
    while i < len(rs):
        j = i+1
        rs1 = rs[i][1]
        while j < len(rs):
            if rs[j][0] != rs[i][0] or rs[j][1] != rs1+1:
                break
            rs1 = rs[j][1]
            j += 1
        if j > i+2:
            rss.append (rs[i][0]+str(rs[i][1])+"–"+str(rs[j-1][1]))
            i = j
        else:
            rss.append (rs[i][0]+str(rs[i][1]))
            i += 1
    return ", ".join(rss)


def rkey(value):
    """Value to sorting key function
    Sorts by color bands, i.e. digits, then magnitude
    e.g.: 100R, 1k, 10k, 1M, 220, 3k, 4.7M, 680
    """

    if (len(value) < 2 or value[-1] >= '0' and value[-1] <= '9'):
        valn = value
        valx = 'R'
    else:
        valn = value[:-1]
        valx = value[-1]
    while valn[0] == '0' and len(valn) > 1:
        valn = valn[1:]
    # Check if not standard format
    if valn[0] < '1' or valn[0] > '9' or re.search ("[^0-9RkM.]", valn):
        return "zzzzzzzz"

    dp = valn.find(".")
    if dp == -1:
        dig = valn
        mag = len(str(dig))-2
    else:
        dig = valn[:dp]+valn[dp+1:]
        mag = dp-2

    if len(dig) < 2:
        dig += '0'
    elif len(dig) > 2:
        dig = dig[:2]
        
    if valx == 'R':
        pass
    elif valx == 'k':
        mag += 3
    elif valx == 'M':
        mag += 6

    return dig+str(mag)

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        'netlist_file',
        type=str,
        help='path to KiCad generated netlist file'
    )
    parser.add_argument(
        'output_file',
        type=str,
        default="bom.md",
        help='path for output'
    )
    parser.add_argument(
        '--todocs',
        action='store_true',
        help='whether to write file to Docs/BOM folder in folder above current'
    )
    parser.add_argument(
        '--rlist',
        action='store_true',
        help='whether to append list of resistors in color band order'
    )

    parser.add_argument(
        '--groups',
        type=str,
        default='(("",(0,100000)))',
        help='tuple of (tuple of name followed by (duple of ref #s))'
        )
    
    args = parser.parse_args()
    fname = args.output_file
    netpath = args.netlist_file
    
    rngroups = str2tuple(args.groups)

    # Start with a basic md template
    md = \
"""# <!--SOURCE--> BOM

<!--DATE-->

Generated from schematic by <!--TOOL-->

<!--COMPCOUNT-->

<!--TABLEROW-->
"""

    # Override the component equivalence operator - it is important to do this
    # before loading the netlist, otherwise all components will have the original
    # equivalency operator.
    kicad_netlist_reader.comp.__eq__ = myEqu

    # Generate an instance of a generic netlist, and load the netlist tree from
    # <file>.tmp. If the file doesn't exist, execution will stop
    net = kicad_netlist_reader.netlist(netpath)

    # Open a file to write to, if the file cannot be opened output to stdout
    # instead
    
    # If --todocs specified try to find Docs directory and use that
    if args.todocs:
        fpath = findDocs(os.path.dirname(fname))
        if fpath == None:
            e = f"No Docs directory found above {os.path.dirname(fname)}"
            print(__file__, ":", e, file=sys.stderr)
        else:
            fpath = os.path.join(fpath, "BOM")
            if not os.path.exists (fpath):
                os.makedirs (fpath)
            fname = os.path.join(fpath, os.path.basename(fname))

    try:
        f = open(fname, 'w')
        e = f"Writing to file {fname}"
        print(__file__, ":", e, file=sys.stderr)
    except IOError:
        e = f"Can't open output file for writing: {fname}"
        print(__file__, ":", e, file=sys.stderr)
        f = sys.stdout

    # Generate table rows

    # Additional fields to print (if nonempty)

    fields = ["Manufacturer", "Part", "Vendor", "SKU", "Note"]
    pfields = [False] * len(fields)

    componentsa = net.getInterestingComponents (excludeBOM=True, DNP=True)

    # Output a set of rows for a header providing general information

    md = md.replace('<!--SOURCE-->', os.path.basename(net.getSource()))
    md = md.replace('<!--DATE-->', net.getDate())
    md = md.replace('<!--TOOL-->', net.getTool())
    md = md.replace('<!--COMPCOUNT-->', f"**Component Count:** {len(componentsa)}")

    sepline = ""
    
    for rng in rngroups:
        [rngname, rng] = [rng[0], rng[1:]]
        if rngname == "" and len(rngroups) > 1:
            rngname = f"Refs in {rng}"
        components = [x for x in componentsa if ingroup (int(re.sub('[^0-9]', '', x.getRef())), rng)]

        # Get all of the components in groups of matching parts + values
        # (see kicad_netlist_reader.py)
        grouped = net.groupComponents(components)

        rows = []
        ncomp = 0

        for group in grouped:
            refs = []

            # Add the reference of every component in the group and keep a reference
            # to the component so that the other data can be filled in once per group
            for component in group:
                refs.append (component.getRef())
                ncomp += 1
                c = component
            if len(refs) == 0:
                continue

            d = c.getField("Description")
            if d=="":
                d = c.getDescription()
            rows.append([refs, str(len(group)), c.getValue(), d])
            for fi in range(len(fields)):
                fv = c.getField(fields[fi])
                rows[-1].append(fv)
                if not fv == "":
                    pfields[fi] = True

        # Output header for the table of components

        md = md.replace('<!--TABLEROW-->', f"\n{sepline}## {rngname}\n" + "<!--TABLEROW-->")
        sepline = "\n----\n"
        row1  = "| Refs | Qty | Component | Description |"
        row2  = "\n| ----- | --- | ---- | ----------- |"

        for fi in range(len(fields)):
            if pfields[fi]:
                row1 += f" {fields[fi]} |"
                row2 += " ---- |"

        md = md.replace('<!--TABLEROW-->', row1 + "<!--TABLEROW-->")
        md = md.replace('<!--TABLEROW-->', row2 + "<!--TABLEROW-->")

        # Output components

        for r in rows:
            row = "\n|"
            for ri in range(4):
                row += f" {ref_format(r[ri]) if ri==0 else r[ri]} |"
            for ri in range(4,len(r)):
                if pfields[ri-4]:
                    row += f" {r[ri]} |"
            md = md.replace('<!--TABLEROW-->', row + "<!--TABLEROW-->")


        
        # Do resistors list
        if args.rlist:
            md = md.replace('<!--TABLEROW-->', "\n\n### Resistors in color band order<!--TABLEROW-->")
            md = md.replace('<!--TABLEROW-->', f"\n|Value|Qty|Refs|<!--TABLEROW-->")
            md = md.replace('<!--TABLEROW-->', f"\n|----|----|----|<!--TABLEROW-->")
            rrows = []
            for r in rows:
                if len(r[0][0]) < 2 or r[0][0][0] != 'R' or (r[0][0][1] < '0' or r[0][0][1] > '9'):
                    continue
                rrows.append(r)
            for r in sorted (rrows, key = lambda row: rkey(row[2])):
                md = md.replace('<!--TABLEROW-->', f"\n|{r[2]}|{r[1]}|{ref_format(r[0])}|<!--TABLEROW-->")
        
    # Print the formatted md to output file
    md = md.replace('<!--TABLEROW-->', "")
    print(md, file=f)

if __name__ == '__main__':
    main()
    
