# KiCad BOM scripts

KiCad plugins for BOMs

Add the Python scripts to your KiCad scripting plugins and use them like any other BOM plugin...

In my Linux installation they go in `.local/share/kicad/7.0/scripting/plugins`. That folder must also contain a copy of (or contain a symlink to) `kicad_netlist_reader.py` which I find in `/usr/share/kicad/plugins`.

## bom_group_md

Grouped BOM in Markdown format

Components are sorted and grouped by value.
Fields included are (if existing) `Refs`, `Quantity`, `Component`, `Description`, `Manufacturer`, `Part`, `Vendor`, `SKU`, and `Note`. Columns after `Description` are suppressed if empty for all components. 

Parts with `Config` field containing "dnf" are omitted. (In KiCad 6+, this isn't needed since there is an "exclude from BOM" check box.)

If there is a user-defined field named `Description` for a component, its contents override the default description for that component. This may be more convenient than going into the symbol editor to change the description.

Command line:
```
python "pathToFile/bom_group_md.py" netlist-file markdown-file [--todocs] [--rlist] [--groups GROUPS] 
```

### --todocs

If --todocs is used, the script searches for a path for the output file like `/project/path/Docs` where `/project/path` is the specified or implicit output directory or one at a higher level. If no such `Docs` directory is found, the specified or implicit path is used instead.

### --rlist

Append list of resistors in color band order — e.g. 100R, 1k, 10k, 100k, 1M, 120R, 12k... 

Expects values consisting of up to two significant figures followed by zeroes, with or without a decimal point, followed by R or k or M. Values not conforming to expected format are put last.

### --groups GROUPS

Separate the BOM into sections by reference numbers. GROUPS is a tuple of (tuples of the form name, duple, duple...) where each duple specifies a range of reference numbers to go into a group with the given name. This can be useful to separate parts going onto different boards of a multi-board design — especially in combination with KiCad's annotation option to use numbers starting from 100 times the page number. For example:
 
`--groups (("Front", (0,100),(300,400)),("Back", (100,300)))`

means to list anything with reference numbers in the ranges [0, 100) or [300, 400) under the name "Front" and anything with reference numbers in the range [100, 300) under the name "Back".

## bom_resistors

Resistors only, sorted by value, in text format.

Command line:
```
python "pathToFile/bom_resistors.py" netlist-file text-file [--xxy] [--todocs]
```

If `--xxy` is used, values are internally converted to 3-digit representation (2 significant figures + number of zeroes, e.g. 124 for 120k) for sorting. Otherwise numerical value (e.g. 120000 for 120k) is used.

If --todocs is used, the script searches for a path for the output file like `/project/path/Docs` where `/project/path` is the specified or implicit output directory or one at a higher level. If no such `Docs` directory is found, the specified or implicit path is used instead.

Expects values consisting of up to two significant figures followed by zeroes, with or without a decimal point, followed by R or k or M. Values not conforming to expected format are put last.

### Known issues

Does not recognize values with format like 1k2 or 4M7.

(Also does not recognize K instead of k, m instead of M, or r instead of R, but that is a feature, not a bug.)
